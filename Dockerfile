FROM tomcat:8.0.20-jre8
COPY /target/lab1.war /usr/local/tomcat/webapps/lab1.war
COPY tomcat-users.xml /usr/local/tomcat/conf/