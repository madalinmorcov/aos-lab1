package com.madalinmorcov.aoslab.controller;


import com.madalinmorcov.aoslab.persistence.entities.Person;
import com.madalinmorcov.aoslab.persistence.repositories.PersonRepoFactory;
import com.madalinmorcov.aoslab.persistence.repositories.PersonRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class ITRegisterServlet {

  @Mock
  private HttpServletRequest request;

  @Mock
  private HttpServletResponse response;

  private PersonRepository repository;

  @Before
  public void setUp() {
    repository = PersonRepoFactory.getPersonRepo();
    request = mock(HttpServletRequest.class);
    response = mock(HttpServletResponse.class);
  }

  @Test
  public void registerNewPerson_Success() {

    String name = "MadalinGeorge_IT";
    given(request.getParameter("name")).willReturn(name);
    given(request.getParameter("email")).willReturn("madalinmorcov@gmail.com");

    RegisterServlet servlet = new RegisterServlet();
    try {
      servlet.doPost(request, response);
    } catch (Exception e) {
      e.printStackTrace();
    }

    Person person = repository.get(name);
    assertThat(person.getName(), is(name));
  }

}
