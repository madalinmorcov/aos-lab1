package com.madalinmorcov.aoslab.persistence.repositories;

public class PersonRepoFactory {

  private static PersonRepository personRepository;
  private static int instances=0;

  public static PersonRepository getPersonRepo(){
    if (instances==0) {
      personRepository = new PersonRepository();
      instances = 1;
    }
    return personRepository;
  }
}
