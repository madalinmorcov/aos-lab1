package com.madalinmorcov.aoslab.controller;

import com.madalinmorcov.aoslab.service.PersonService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet({"/","/index"})
public class RegisterServlet extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, ServletException {
    httpServletRequest.getRequestDispatcher("WEB-INF/index.jsp").forward(httpServletRequest, httpServletResponse);
  }

  @Override
  public void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, ServletException {
    String name = httpServletRequest.getParameter("name");
    String email = httpServletRequest.getParameter("email");
    PersonService service = new PersonService();
    service.add(name,email);
    httpServletResponse.sendRedirect("/register");
  }
}
